# Nord-Sud

Nord-Sud is a typeface originally created by by [Ariel Martín Pérez](http://arielgraphisme.com/) in 2017, and now released through [Tunera Type Foundry](http://tunera.xyz).

![specimen1](documentation/specimen/specimen-nordsud-04.jpg)

Nord-Sud is an historical display typeface, that allows to faithfully reproduce the look of the tile signs of the ancient Nord-Sud train company through the combination of several text layers. 

If you use Nord-Sud in any of your projects, you must credit the name of the font and of its original author like this "Typeface: Nord-Sud by Ariel Martín Pérez". If you're unable to include the credits on your project, due to a reduced print surface, for instance, you should include the credits when you communicate about your project on social media or on your portfolio.

## Specimen

![specimen1](documentation/specimen/specimen-nordsud-01.jpg)
![specimen1](documentation/specimen/specimen-nordsud-02.jpg)
![specimen1](documentation/specimen/specimen-nordsud-03.jpg)

## License

Nord-Sud is licensed under the SIL Open Font License, Version 1.1.
This license is copied below, and is also available with a FAQ at
http://scripts.sil.org/OFL

## Repository Layout

This font repository follows the Unified Font Repository v2.0,
a standard way to organize font project source files. Learn more at
https://github.com/raphaelbastide/Unified-Font-Repository
